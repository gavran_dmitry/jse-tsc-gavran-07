package ru.tsc.gavran.tm;

import ru.tsc.gavran.tm.constant.ArgumentConst;
import ru.tsc.gavran.tm.constant.TerminalConst;
import ru.tsc.gavran.tm.model.Command;
import ru.tsc.gavran.tm.repository.CommandRepository;
import ru.tsc.gavran.tm.util.NumberUtil;

import java.util.Arrays;
import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        parseArgs(args);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    public static void parseArg(final String arg) {
        switch (arg) {
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.INFO:
                showInfo();
                break;
            default: showErrorArgument();

        }
    }

    public static void parseCommand(final String command) {
        switch (command) {
            case TerminalConst.ABOUT: showAbout(); break;
            case TerminalConst.VERSION: showVersion(); break;
            case TerminalConst.HELP: showHelp(); break;
            case TerminalConst.INFO: showInfo(); break;
            case TerminalConst.EXIT: exit(); break;
            default: showErrorCommand();

        }
    }

    public static void showErrorArgument () {
        System.err.println("Error! Argument not supported...");
        System.exit(1);
    }

    public static void showErrorCommand () {
        System.err.println("Error! Command not found...");
    }

    public static void parseArgs(String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }

    public static void exit() {
        System.exit(0);
    }

    public static void showInfo() {
        System.out.println("[INFO]");
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory: " + NumberUtil.formatBytes(usedMemory));
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("DEVELOPER: Dmitry Gavran");
        System.out.println("E-MAIL: dgavran@tsconsulting.com");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = CommandRepository.getCommands();
        for (Command command: commands) System.out.println(command);
    }

}